# sem-track

#### 介绍
竞价推广中，常用的转化回传类

#### 安装教程

1.  安装扩展：`composer require iwesen/sem-track`
2.  发布配置：`php artisan vendor:publish --provider="Iwesen\Sem\Track\SemTrackServiceProvider"`
3.  修改配置：根据情况配置各渠道默认转化类型或账户`token`

#### 使用说明

#### 调用方法
1. 广告商支持(`$advertiser`)
`jinritoutiao`=>`今日头条`;
`baidu`=>`百度`;
`gdt`=>`广点通`;
`uctoutiao`=>`UC头条`;
`sm`=>`神马搜索`;
`meiyou`=>`美柚`;
`sougou`=>`搜狗`;
`qihu`=>`奇虎`;
`kuaishou`=>`快手`;
`zhihu`=>`知乎`;

2. 调用示例
##### 转化回传
```
use Iwesen\Sem\Track\SemTrack;
……
$semTrack = new SemTrack;
$res = $semTrack->send($advertiser,$param)
……
```
##### 广点通授权
```
use Iwesen\Sem\Track\GdtOauth;
……
$res = (new GdtOauth())->getToken();
……
```
返回数据格式如下：
```
array:5 [▼
  "authorizer_info" => array:5 [▼
    "account_uin" => 781470***
    "account_id" => 1461***
    "scope_list" => array:1 [▼
      0 => "user_actions"
    ]
    "wechat_account_id" => ""
    "account_role_type" => "ACCOUNT_ROLE_TYPE_AGENCY"
  ]
  "access_token" => "5c54***"
  "refresh_token" => "7cbc***"
  "access_token_expires_in" => 86400
  "refresh_token_expires_in" => 2592000
]
```
获取数据后，自行保存到数据库，另外，需要自行添加定时任务，刷新`access_token`

3. 各广告商的`$param`如下说明：

##### 今日头条(`jinritoutiao`)
1. `$advertiser`:`jinritoutiao`
2. `$param`格式：
```
$param = [
    'link'=>'',//着陆页url，与clickid必选其一
    'clickid'=>'',//用户点击id，与clickid必选其一
    'event_type'=>'',//默认配置或必填，参考值：https://ad.oceanengine.com/openapi/doc/index.html?id=556
]
```

##### 百度(`baidu`)
1. `$advertiser`:`baidu`
2. `$param`格式：
```
$param = [
    'token'=>''//默认配置或必填
    'conversionTypes'=>[
        [
            'logidUrl'=>''//必填：包含bd_vid的落地页url
            'newType'=>''//默认配置或必填,参考值：http://ocpc.baidu.com/developer/ocpc-doc/api/api-faq/converttype.html
        ],
        [
           'logidUrl'=>''，
           'newType'=>''
        ]，
        [
            ……
        ]

    ]
]
```


##### 广点通(`gdt`)
1. `$advertiser`:`gdt`
2. `$param`格式：
```
$param = [
    'account_id'=>'',//账户ID
    'access_token'=>'',//有效的账户授权token
    'actions'=>array(
        [
            'user_action_set_id'=>'',//账户用户行为数据源id
            'url'=>'',//行为发生页面url
            'action_time'=>time(),
            'trace'=>[
                'click_id'=>'',//用户点击id，对于广点通流量为URL中的参数`qz_gdt`的值，对于微信流量为URL中的参数`gdt_vid`的值
            ],
            'action_type'=>'',//转化目标类型，参考值：https://developers.e.qq.com/docs/reference/enum#api_action_type
            'action_param'=>[
                'value'=>'',//转化价值
            ]
        ],
    )
];
```

##### UC头条(`uctoutiao`)
1. `$advertiser`:`uctoutiao`
2. `$param`格式：
```
$param = [
    'link'=>''//必填：着陆页url
    'event_type'=>''//默认配置或必填，参考值：https://yingxiao.uc.cn/article_208.html
]
```

##### 神马搜索(`sm`)
1. `$advertiser`:`sm`
2. `$param`格式：
```
$param = [
    'username'=>'',//账户
    'password'=>'',//密码
    'source'=>'',//标识数据的来源
    'data'=>[
        [
            'click_id'=>'',//点击ID，必填
            'conv_type'=>'',//转化类型，默认或必填
            'conv_name'=>'',//转化名称，默认或必填
            'conv_value'=>'',//转化值，默认或必填
        ],
    ]
]
```

##### 美柚女人通(`meiyou`)
1. `$advertiser`:`meiyou`
2. `$param`格式：
```
$param = [
    'callback'=>'',//着陆页中的callback原值，不要urldecode!
    'conv_type'=>'',//枚举：14-在线下单； 15-表单 提交；
]
```

##### 搜狗(`sougou`)
1. `$advertiser`:`sougou`
2. `$param`格式：
```
$param = [
    'token'=>''//默认配置或必填
    'conversions'=>[
        [
            'logidUrl'=>''//必填：包含sg_vid的落地页url
            'convertType'=>''//默认配置或必填,参考搜狗ocpc文档
        ],
        [
           'logidUrl'=>''，
           'convertType'=>''
        ]，
        [
            ……
        ]

    ]
]
```

##### 奇虎(`qihu`)
1. `$advertiser`:`qihu`
2. `$param`格式：
```
$param = [
    'data_industry'=>'',//必填,参考Iwesen\Sem\Track\Advertisers\Qihu
    'impression_id'=>'',//data_industry=ocpc_web_convert时即移动推广，必填且非空
    'qhclickid'=>'',//搜索和展示广告必填
    'jzqs'=>'',//展示广告，必填且非空，向支持人员获取该id
    'trans_id'=>'',//转化id，建议传客户id或者订单id,提交前会进行md5
    'event'=>'',//转化类型，必填
    'event_time'=>'',//转化时间(秒级时间戳)，不传将自动填现在的时间
]
```

##### 快手(`kuaishou`)
1. `$advertiser`:`kuaishou`
2. `$param`格式：
```
$param = [
    'eventType'=>9,//必填,转化类型,9:表单提交,44:有效获客,69:教育正价课付费
    'callback'=>'',//转化ID,创建转化追踪后生成的callback
    'purchaseAmount'=>'',//当转化类型为69时必填，成单金额
]
```

##### 知乎(`zhihu`)
1. `$advertiser`:`zhihu`
2. `$param`格式：
```
$param = [
    'link'=>'',//知乎宏替换后的__CALLBACK__
    'event_type'=>'api_submit',//必填,转化类型
    'event_value'=>'',//转化值，可空
]
```

