<?php

namespace Iwesen\Sem\Track;

use Illuminate\Support\ServiceProvider;

class SemTrackServiceProvider extends ServiceProvider
{
    /**
     * {@inheritdoc}
     */
    public function boot()
    {
        $this->publishes([__DIR__ . '/../config/sem-track.php' => config_path('sem-track.php')], 'sem-track');
    }

    /**
     * 注册服务
     */
    public function register(): void
    {
        $this->app->singleton(SemTrack::class, function () {
            return new SemTrack();
        });
        $this->app->singleton(GdtOauth::class, function () {
            return new GdtOauth();
        });
    }
}
