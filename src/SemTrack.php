<?php

namespace Iwesen\Sem\Track;

class SemTrack {

     //目前受支持的广告商
    const ADVERTISERS = array(
        'jinritoutiao'  =>'今日头条',//今日头条
        'baidu'         =>'百度推广',//百度
        'gdt'           =>'广点通',//广点通
        'uctoutiao'     =>'UC头条',//UC头条
        'sm'            =>'神马搜索',//神马搜索
        'meiyou'        =>'美柚APP',//美柚APP
        'sougou'        =>'搜狗搜索',//搜狗搜索
        'qihu'          =>'360',//奇虎360
        'kuaishou'      =>'快手',//快手
        'zhihu'         =>'知乎',
    );

    /**
     * @param string $advertiser 'jinritoutiao','baidu','gdt',
     * @param array  $param
     */
    public function send($advertiser,$param)
    {
        if(!in_array($advertiser,array_keys(self::ADVERTISERS))){
            return '暂不支持该广告商~';
        }
        $advertiser = ucwords(str_replace(['-', '_'], ' ', $advertiser));
        $application = "\\Iwesen\\Sem\\Track\\Advertisers\\".$advertiser;

        return (new $application())->callback($param);
    }
}