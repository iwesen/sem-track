<?php

namespace Iwesen\Sem\Track\Advertisers;

class Meiyou
{
   
    public function callback($param)
    {
        if(!isset($param['callback']) || !$callback = $param['callback']){
            throw new \Exception( 'callback is not a exist !');
        }

        if(!isset($param['conv_type']) || !$conv_type = $param['conv_type']){
            $conv_type = config('sem-track.meiyou.conv_type');
            if(!$conv_type){
                throw new \Exception( 'conv_type is not a exist !');
            }
        }
        try {
            $time = time();
            $callback = urldecode($param['callback']);
            $url = $callback."&conv_type={$conv_type}&conv_time={$time}";
            $result = (new Request())->_request($url);
            $data = json_decode($result,true);
            if($data['error_code']==0){
                return 'success';
            }
            return 'fail';
        } catch (\Exception $e) {
            throw new \Exception($e->getMessage(), $e->getCode(), $e);
        } 
    }
}