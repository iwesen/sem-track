<?php

namespace Iwesen\Sem\Track\Advertisers;

class Uctoutiao
{
    protected $api =  "https://huichuan.uc.cn/callback/ct/add";

    public function callback($param)
    {
        if(!isset($param['link']) || !$link = $param['link']){
            throw new \Exception( 'link is not  exist !');
        }
        if(!isset($param['event_type']) || !$event_type = $param['event_type']){
            $event_type = config('sem-track.uctoutiao.event_type');
            if(!$event_type){
                throw new \Exception( 'event_type is not  exist !');
            }
        }
        try {
            $time = time();
            $link = str_replace("+","%09",urlencode($link));//UC的uctrackid值中的空格为tab而不是空格
            $url = $this->api."?link={$link}&event_time={$time}&event_type={$event_type}&source=";
            $result = (new Request())->_request($url);
            $data = json_decode($result,true);
            if($data['status']==0){
                return 'success';
            }
            return 'fail';
        } catch (\Exception $e) {
            throw new \Exception($e->getMessage(), $e->getCode(), $e);
        } 
    }
}