<?php

namespace Iwesen\Sem\Track\Advertisers;

use Illuminate\Support\Facades\Log;

class Qihu
{
    protected $api =  "https://convert.dop.360.cn/uploadWebConvert";

    const INDUSTRY_PS   = 'ocpc_ps_convert';//PC搜索
    const INDUSTRY_MS   = 'ocpc_ms_convert';//移动搜索
    const INDUSTRY_WEB  = 'ocpc_web_convert';//移动推广
    const INDUSTRY_ZS   = 'ocpc_zs_convert';//展示广告

    //列举常用的转化类型
    const EVENT_SUBMIT      = 'SUBMIT';//成功提交表单内容
    const EVENT_CALL        = 'CALL';//有效电话拨打
    const EVENT_ADVISORY    = 'ADVISORY';//一次咨询对话中，用户发出大于等于 1 条内容
    const EVENT_ORDER       = 'ORDER';//成功产生一次订单--移动推广不支持


    public function callback($param)
    {
        if(!isset($param['key']) || !$param['key']){
            $param['key'] = config('sem-track.qihu.key');
            if(!$param['key']){
                throw new \Exception( 'key does not exist in parameter !');
            }
        }
        if(!isset($param['secret']) || !$param['secret']){
            $param['secret'] = config('sem-track.qihu.secret');
            if(!$param['secret']){
                throw new \Exception( 'secret does not exist in parameter !');
            }
        }

        if(!isset($param['data_industry']) || !$param['data_industry']){
            $param['data_industry'] = config('sem-track.qihu.data_industry');
            if(!$param['data_industry']){
                throw new \Exception( 'data_industry does not exist in parameter !');
            }
        }

        if(!isset($param['event']) || !$param['event']){
            $param['event'] = config('sem-track.qihu.event');
            if(!$param['event']){
                throw new \Exception( 'event does not exist in parameter !');
            }
        }

        if(!isset($param['impression_id']) && $param['data_industry']==self::INDUSTRY_WEB){
            throw new \Exception( 'impression_id does not exist in parameter !');
        }

        if(!isset($param['qhclickid']) && in_array($param['data_industry'],[self::INDUSTRY_ZS,self::INDUSTRY_PS,self::INDUSTRY_MS])){
            throw new \Exception( 'qhclickid does not exist in parameter !');
        }

        if($param['data_industry']==self::INDUSTRY_ZS && (!isset($param['jzqs']) || !$param['jzqs'])){
            $param['jzqs'] = config('sem-track.qihu.jzqs');
            if(!$param['jzqs']){
                throw new \Exception( 'jzqs does not exist in parameter !');
            }
        }

        if(isset($param['trans_id']) && $param['trans_id']){
            $param['trans_id'] = MD5($param['trans_id']);
        }

        $dataDetail=[
            'impression_id'=>isset($param['impression_id'])?$param['impression_id']:'',
            'qhclickid'=>isset($param['qhclickid'])?$param['qhclickid']:'',
            'jzqs'=>isset($param['jzqs'])?$param['jzqs']:'',
            'trans_id'=>isset($param['trans_id'])?$param['trans_id']:time(),
            'event'=>$param['event'],
            'event_time'=>isset($param['event_time'])?$param['event_time']:time(),
        ];
        $requestData=[
            'data'=>[
                'request_time'=>time(),
                'data_industry'=>$param['data_industry'],
                'data_detail'=>array_filter($dataDetail)
            ],
        ];

        $header=[
            'App-Key:'.$param['key'],
            'App-Sign:'.md5($param['secret'].json_encode($requestData)),
            'Content-Type: application/json; charset=utf-8'
        ];

        try {
            $result = (new Request())->_request($this->api,$header,$requestData);
            $data = json_decode($result,true);
            $status = $data['errno'];
            if ($status === 0) {
                return 'success';
            }
            return $data['error'];
        } catch (\Exception $e) {
            throw new \Exception($e->getMessage(), $e->getCode(), $e);
        }
    }
}