<?php

namespace Iwesen\Sem\Track\Advertisers;

use Illuminate\Support\Facades\Log;

class Sougou
{
    protected $api =  "https://ocpc.p4p.sogou.com/ocpc/api/uploadConvertData";

    public function callback($param)
    {
        if(!isset($param['token']) || !$param['token']){
            $param['token'] = config('sem-track.sougou.token');
            if(!$param['token']){
                throw new \Exception( 'token is not a exist !');
            }
        }
        if(!isset($param['conversions']) || !is_array($param['conversions'])){
            throw new \Exception( 'conversions is not a exist !');
        }
        //验证转化类型并补全
        foreach($param['conversions'] as $k=>$cv){
            if(!is_array($cv)){
                throw new \Exception( 'conversion presentation error !');
            }
            if(!isset($cv['convertType'])||!$cv['convertType']){
                $param['conversions'][$k]['convertType']=config('sem-track.sougou.convertType');
            }
        }
        try {
            $result = (new Request())->_request($this->api,'',$param);
            $data = json_decode($result,true);
            $status = $data['status'];
            if ($status === 200) {
                return 'success';
            }
            return $data['errorInfoList'][0]['errorMsg'];
        } catch (\Exception $e) {
            throw new \Exception($e->getMessage(), $e->getCode(), $e);
        }
    }
}