<?php

namespace Iwesen\Sem\Track;

use Iwesen\Sem\Track\Advertisers\Request;


class GdtOauth
{

    /**
     * 应用ID
     */
    protected $client_id;
    /**
     * 应用密钥
     */
    protected $client_secret;

    /**
     * 应用回调地址
     */
    protected $redirect_uri;

    public function __construct()
    {
        $this->client_id = config('sem-track.gdt.client_id'); 
        $this->client_secret =config('sem-track.gdt.client_secret'); 
        $this->redirect_uri = config('sem-track.gdt.redirect_uri'); 
    }

    public function getToken($state=null)
    {
        $code =request('authorization_code'); 
        if (!$code) {
            $url = $this->__CreateOauthUrlForCode($state);
            Header("Location: $url");
            //exit();
        } else {
            //换取token
            $token = $this->getTokenFromQq($code);
            return $token;
        }
    }

    /**
     * 刷新token
     */
    public function refreshToken($refresh_token)
    {
        $url = $this->__CreateOauthUrlForRefreshToken($refresh_token);
        $res = (new Request())->_request($url);
        $data = json_decode($res, true);
        if($data['code']===0){
            return $data['data'];
        }
        return $data['message'];
    }

    /**
     * 通过authorization_code从平台获取access_token和refresh_token
     * @param string $code QQ跳转回来带上的authorization_code
     * @return tokenArr
     */
    public function getTokenFromQq($code)
    {
        $url = $this->__CreateOauthUrlForToken($code);
        $res = (new Request())->_request($url);
        $data = json_decode($res, true);
        if($data['code']===0){
            return $data['data'];
        }
        return $data['message'];
    }

    /**
     * 构造获取access_token和refresh_token的url地址
     * @param string $code，QQ跳转带回的code
     * @return 请求的url
     */
    private function __CreateOauthUrlForToken($code)
    {
        $urlObj["client_id"] = $this->client_id;
        $urlObj["client_secret"] = $this->client_secret;
        $urlObj["grant_type"] = "authorization_code";
        $urlObj["authorization_code"] = $code;
        $urlObj["redirect_uri"] = $this->redirect_uri;
        $bizString = $this->ToUrlParams($urlObj);
        return "https://api.e.qq.com/oauth/token?".$bizString;
    }

    /**
     * 构造获取access_token和refresh_token的url地址
     * @param string $code，QQ跳转带回的code
     * @return 请求的url
     */
    private function __CreateOauthUrlForRefreshToken($refresh_token)
    {
        $urlObj["client_id"] = $this->client_id;
        $urlObj["client_secret"] = $this->client_secret;
        $urlObj["grant_type"] = "refresh_token";
        $urlObj["refresh_token"] = $refresh_token;
        $bizString = $this->ToUrlParams($urlObj);
        return "https://api.e.qq.com/oauth/token?".$bizString;
    }

    /**
     * 构造获取authorization_code的url连接
     * @param string $redirectUrl 回跳的url，需要url编码
     * @return 返回构造好的url
     */
    private function __CreateOauthUrlForCode($state=null)
    {
        $urlObj["client_id"] = $this->client_id;
        $urlObj["redirect_uri"] = $this->redirect_uri;
        $urlObj["account_type"] = "ACCOUNT_TYPE_QQ";
        $urlObj["scope"] = "";
        $urlObj["state"] = $state;
        $bizString = $this->ToUrlParams($urlObj);
        return "https://developers.e.qq.com/oauth/authorize?".$bizString;
    }
    
    /**
     * 拼接签名字符串
     * @param array $urlObj
     * @return 返回已经拼接好的字符串
     */
    private function ToUrlParams($urlObj)
    {
        $buff = "";
        foreach ($urlObj as $k => $v) {
            if ($k != "sign") {
                $buff .= $k . "=" . $v . "&";
            }
        }
        $buff = trim($buff, "&");
        return $buff;
    }
}