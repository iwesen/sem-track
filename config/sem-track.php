<?php

return [
    'jinritoutiao'=>[
        'event_type'    =>19,//转化类型参考：https://ad.oceanengine.com/openapi/doc/index.html?id=556
    ],
    'baidu'=>[
        'token'=>'',
        'newType'=>19,//转化类型参考：http://ocpc.baidu.com/developer/ocpc-doc/api/api-faq/converttype.html
    ],
    'gdt'=>[
        'client_id'=>env('GDT_CLIENT_ID',''),
        'client_secret'=>env('GDT_CLIENT_SECRET',''),
        'redirect_uri'=>env('GDT_REDIRECT_URI',''),
    ],
    'uctoutiao'=>[
        'event_type'    =>1002,//转化类型参考：https://yingxiao.uc.cn/article_208.html
    ],
    'sm'=>[
        'username'=>'',//账户名
        'password'=>'',//账户密码
        'source'=>0,//标识数据的来源0广告主数据，1第三方数据，3 第三方工具
        'conv_type'=>13,// 枚举对象：1 - 激活 ；2 - 下载；3 - 浏览关键词页面；5 – 表单提交；6 - 拨打电话；10 - 提交订单；11 - 购买； 12 - 注册； 13 - 在线咨询； 15- 访客数；   14 - 其他；
        'conv_name'=>'在线咨询',
        'conv_value'=>1,
    ],
    'meiyou'=>[
        'conv_type'=>15,//枚举：14-在线下单； 15-表单 提交；
    ],
    'sougou'=>[
        'token'=>'',
        'convertType'=>18,//转化类型参考搜狗ocpc文档
    ],
    'qihu'=>[
        'key'=>'',
        'secret'=>'',
        'data_industry'=>'ocpc_ps_convert',//参考Iwesen\Sem\Track\Advertisers\Qihu，默认为PC搜索
        'jzqs'=>'',
        'event'=>'ADVISORY',//参考Iwesen\Sem\Track\Advertisers\Qihu,默认为一句话咨询
    ],
    'kuaishou'=>[
        'eventType'=>9,//转化类型 枚举：9-表单提交； 15-表单 提交；
        'convertId'=>'',//转化ID
    ]
];